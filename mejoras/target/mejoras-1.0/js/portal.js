$(document).ready(function () {
    
    $('#solicitud').formSelect();
    $('.modal').modal();
    
    $('#comentarios').text("\n\n\n\n\n\n\n\n");
    M.textareaAutoResize($('#comentarios'));
    
    $('#exit').click(function(){
        window.location.replace('index.jsp');
    });
    
    $('span').hover(function () {
        if ($(this).text() === 'Mejora') {
            var info = 'Es una propuesta de mejora a un modulo existente';
            M.toast({html: info});
        } else if ($(this).text() === 'Bug/Falla') {
            var info = '<p>Error encontrado en algun modulo del sistema:&nbsp;&nbsp;</p><br>\n';
            info += '<p><span class="red-text">Critico&nbsp;</span>\n';
            info += '<span class="yellow-text">Importante&nbsp;</span>';
            info += '<span class="green-text">Normal&nbsp;</span></p>';
            M.toast({html: info});
        } else if ($(this).text() === 'Nuevo Módulo') {
            var info = 'Es una propuesta de una nueva utilidad no encontrada en el sistema';
            M.toast({html: info});
        }
    }, function () {
        M.Toast.dismissAll();
    });
    
    
});
