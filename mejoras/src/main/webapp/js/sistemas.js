$(document).ready(function () {
    $('#responsable').formSelect();
    $('.modal').modal();
    $('.datepicker').datepicker();
    $('.datepicker').on('open', function () {
        $('.datepicker').appendTo('#fecha-display');
    });
    $('#status').formSelect();
    $('#exit').click(function(){
        window.location.replace("index.jsp");
    });
});
