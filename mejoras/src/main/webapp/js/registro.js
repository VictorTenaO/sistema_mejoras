$(document).ready(function () {
    $('#hide').hide();
    $('.modal').modal();

    $('#selector').change(function () {
        $('#hide').show();
        $('#change').empty();
        $('#hidden').prop('hidden', false);
        var html_div = '<div class="row" id="var-div">\n';
        if ($(this).val() === '1') {
            html_div += '<div class="col s3 offset-s3 input-field space">\n';
            html_div += '<input id="dependencia" type="text" class="validate">\n';
            html_div += '<label for="dependecia">Dependencia</label>\n';
            html_div += '</div>\n';
            html_div += '<div class="col s3 input-field space">\n';
            html_div += '<input id="departamento" type="text" class="validate">\n';
            html_div += '<label for="departamento">Departamento</label>\n';
            html_div += '</div>\n';
            html_div += '<div class="col s6 offset-s3 input-field space">\n';
            html_div += '<input id="telefono" type="number" class="validate">\n';
            html_div += '<label for="telefono">Telefono</label>\n';
            html_div += '</div>\n';
        } else {
            html_div += '<div class="col s6 offset-s3 input-field space">\n';
            html_div += '<select id="puesto">\n';
            html_div += '<option value="" disabled selected>Seleccionar Puesto</option>';
            html_div += '<option value="directivo">Directivo</option>\n';
            html_div += '<option value="coordinador">Coordinador</option>\n';
            html_div += '<option value="operativo">Operativo</option>\n';
            html_div += '<option value="administrativo">Administrativo</option>\n';
            html_div += '<option value="mesa">Mesa de ayuda</option>\n';
            html_div += '</select>';
            html_div += '</div>\n';
            html_div += '<div class="col s6 offset-s3 input-field space">\n';
            html_div += '<select id="proyecto">\n';
            html_div += '<option value="" disabled selected>Seleccionar Proyecto</option>';
            html_div += '<option value="2">Michioacán</option>\n';
            html_div += '<option value="1">ISEM</option>\n';
            html_div += '</select>';
            html_div += '</div>\n';
            html_div += '<div class="col s6 offset-s3 input-field space">\n';
            html_div += '<select id="jurisdiccion">\n';
            html_div += '<option value="" disabled selected>Seleccionar Jurisdicción</option>';
            html_div += '</select>';
            html_div += '</div>\n';
        }
        html_div += '<div class="col s4 offset-s5 right-align"> \n <a class="btn modal-trigger" href="#modal-response">Enviar datos</a> </div>';
        html_div += '</div>';
        $('#change').append(html_div);
        $('#puesto').formSelect();
        $('#jurisdiccion').formSelect();
        $('#proyecto').formSelect();
    });
});
