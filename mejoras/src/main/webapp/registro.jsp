<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    HttpSession sesion = request.getSession(false);

    if (sesion != null) {
        sesion.invalidate();
    }

    String generalContext = request.getContextPath();
    request.setAttribute("generalContext", generalContext);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MEDALFA</title>
        <%@include file="/jspf/librerias_css.jspf" %>  
        <link rel="stylesheet" href="css/registro.css">
    </head>
    <body>

        <div class="col s10 offset-s1 center-align">
            <div class="row">
                <div class="col s12">
                    <h3 class="teal-text">Portal de Mejoras y Reportes de Fallas de Sistemas</h3>
                    <h5>Departamento de Desarrollo de Software</h5>
                    <h4>Registro</h4>
                </div>
            </div>
        </div>
        <div class="row" id="cont-box">
            <div class="input-field col s4 offset-s4">
                <select id="selector">
                    <option value="" disabled selected>Seleccionar procedencia</option>
                    <option value="1">Funcionario de Secretar&iacute;a de Salud</option>
                    <option value="2">Medalfa</option>
                </select>
            </div>
            <div id="hide">
                <div class="input-field col s3 offset-s3 space">
                    <input id="email" type="email" class="validate">
                    <label for="email">Usuario</label>
                </div>
                <div class="input-field col s3  space">
                    <input id="nombre" type="text" class="validate">
                    <label for="nombre">Nombre Completo</label>
                </div>
                <div class="input-field col s3 offset-s3 space">
                    <input id="password" type="password" class="validate">
                    <label for="password">Contrase&ntilde;a</label>
                </div>
                <div class="input-field col s3 space">
                    <input id="password-conf" type="password" class="validate">
                    <label for="password-conf">Confirme Contrase&ntilde;a</label>
                </div>
                <div id="change">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s2 offset-s8 right-align"> 
                <a href="index.jsp" class="teal-text">Regresar</a>
            </div>
        </div>
        <div class="modal" id="modal-response">
            <div class="modal-content center-align">
                <h6>Datos Enviados</h6>
                <hr>
                <br>
                <h3 class="teal-text">¡Tu registro ha sido exitoso!</h3>
            </div>
            <div class="modal-footer center-align">
                <a href="#!" class="modal-close waves-effect waves-green green btn-flat">Aceptar</a>
            </div>
        </div>
        <%@include file="/jspf/librerias_js.jspf" %>
        <script src="js/registro.js" type="text/javascript"></script>
        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function () {
                var elems = document.querySelectorAll('select');
                var instances = M.FormSelect.init(elems);
            });
        </script>
    </body>
</html>
