<%
    String generalContext = request.getContextPath();
    request.setAttribute("generalContext", generalContext);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Portal de usuario</title>
        <%@include file="/jspf/librerias_css.jspf" %> 
        <link rel="stylesheet" href="css/portal.css">
    </head>
    <body>
        <div class="row">
            <div class="row" id="user-div">
                <div class="col s2 offset-s1 left-align">
                    <p class="">Bienvenido:&nbsp;<span class="teal-text" id="nombre-usuario">nombre usuario</span> </p>
                </div>
                <div class="col s2 offset-s6 right-align" id="salir">
                    <a class="btn grey lighten-3 teal-text" id="exit"><i class="material-icons left teal-text">exit_to_app</i>Salir</a>
                </div>
            </div>
            <div class="row" id="heading-div">
                <div class="col s12 center-align">
                    <h3 class="teal-text">Portal de Mejoras y Reportes de Fallas de Sistemas</h3>
                </div>
            </div>
            <div class="row">
                <div class="col s10 offset-s1 center-align">
                    <h5>Departamento de Desarrollo de Software Portal:&nbsp;<span class="teal-text"> Sistemas</span></h5>
                </div>
            </div>
            <div class="row">
                <br>
                <div class="col s10 offset-s1">
                    <table class="striped centered">
                        <thead>
                            <tr>
                                <th>No. Folio</th>
                                <th>Dependencia/ Procedencia</th>
                                <th>Fecha Solicitud</th>
                                <th>Nombre Solicitante</th>
                                <th>Sistema</th>
                                <th>Tipo Solicitud</th>
                                <th>Descripcion</th>
                                <th>Status</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>0001</td>
                                <td>Jurisdiccion Zamora</td>
                                <td>10-10-2019</td>
                                <td>Nombre Ejemplo</td>
                                <td>Farmacia</td>
                                <td>Modulo</td>
                                <td>
                                    <a class="btn grey lighten-3 teal-text modal-trigger" href="#modal1"> Ver </a>
                                </td>
                                <td class="teal-text">
                                    <a href="#modal2" class=" modal-trigger teal-text btn grey lighten-3">Asignar</a>
                                </td>
                            </tr>
                            <tr>
                                <td>0002</td>
                                <td>Jurisdiccion Morelia</td>
                                <td>10-05-2019</td>
                                <td>Nombre Ejemplo Apellido</td>
                                <td>Pictogr&aacute;fico</td>
                                <td>Bug/falla</td>
                                <td>
                                    <a class="btn grey lighten-3 teal-text modal-trigger" href="#modal1"> Ver </a>
                                </td>
                                <td>
                                    <a href="#modal2" class="btn grey lighten-3 green-text modal-trigger">Proceso</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="modal2" class="modal">
                <div class="modal-content left-align">
                    <h6>Crear una respuesta</h6>
                    <hr>
                    <br>
                    <div class="row center-align">
                        <div class="center-align col s10 offset-s1">
                            <select id="responsable">
                                <option value="" disabled selected>Seleccionar Responsable</option>
                                <option value="sebas">Sebasti&aacute;n</option>
                                <option value="americo">Am&eacute;rico</option>
                                <option value="victor">Victor</option>
                            </select>
                        </div>
                        <div class="col s10 offset-s1">
                            <input type="text" id="fecha-entrega" class="datepicker">
                            <label for="fecha-entrega">Fecha de Entrega Aprox</label>
                        </div>
                        <div id="fecha-display">
                            
                        </div>
                        <div class="input-field col s10 offset-s1">
                            <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                            <label for="icon_prefix2">Escribe una respuesta</label>
                        </div>
                        <div class="center-align col s10 offset-s1">
                            <select id="status">
                                <option value="" disabled selected>Estatus</option>
                                <option value="0">Pendiente</option>
                                <option value="1">Activa</option>
                                <option value="2">Terminada</option>
                            </select>
                        </div>
                        <div class="row">
                            <br>
                            <div class="file-field input-field col s10 offset-s1">
                                <div class="btn grey lighten-3 teal-text">
                                    <i class="material-icons teal-text left">photo</i>
                                    <span>Adjuntar</span>
                                    <input type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" id="evidencia" type="text">
                                    <label for="evidencia">Cargar Archivo/Imagen</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="col s2 offset-s5">
                            <a href="#!" class="waves-effect waves-green grey lighten-3 teal-text btn">Enviar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modal1" class="modal">
                <div class="modal-content center-align">
                    <h4>Descripci&oacute;n</h4>
                    <hr>
                    <h6>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </h6>
                </div>
                <div class="modal-footer center-align">
                    <a href="#!" class="modal-close waves-effect waves-green green btn-flat">Aceptar</a>
                </div>
            </div>
        </div>
        <%@include file="jspf/librerias_js.jspf" %>
        <script src="js/sistemas.js" type="text/javascript"></script>
    </body>
</html>
