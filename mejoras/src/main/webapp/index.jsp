<%
    HttpSession sesion = request.getSession(false);

    if (sesion != null) {
        sesion.invalidate();
    }

    String generalContext = request.getContextPath();
    request.setAttribute("generalContext", generalContext);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Portal de mejoras</title>
        <%@include file="/jspf/librerias_css.jspf" %>  
    </head>
    <body>
        <div class="row" id="content">
            <div class="col s10 offset-s1 center-align">
                <div class="row">
                    <div class="col s12">
                        <h3 class="teal-text">Portal de Mejoras y Reportes de Fallas de Sistemas</h3>
                        <h5>Departamento de Desarrollo de Software</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col s8 offset-s2">
                        <img src="static/logo_app.png" class="responsive-img" alt="logo medalfa">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4 offset-s4">
                        <input id="email" type="email" class="validate">
                        <label for="email">Usuario</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4 offset-s4">
                        <input id="password" type="password" class="validate">
                        <label for="password">Contrase&ntilde;a</label>
                    </div>
                </div>
                <div class="row right-align">
                    <div class="col s3 offset-s5">
                        <a href="registro.jsp" class="teal-text">Registrate</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col s2 offset-s5">
                        <a class="btn" id="login">Iniciar Sesi&oacute;n</a>
                    </div>
                </div>
            </div>
        </div>
         <%@include file="jspf/librerias_js.jspf" %>
         <script type="text/javascript" src="js/index.js"></script>
    </body>
</html>
