<%-- 
    Document   : portal
    Created on : Oct 10, 2019, 9:59:15 AM
    Author     : LENOVO
--%>
<%
    String generalContext = request.getContextPath();
    request.setAttribute("generalContext", generalContext);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Portal de usuario</title>
        <%@include file="/jspf/librerias_css.jspf" %> 
        <link rel="stylesheet" href="css/portal.css">
    </head>
    <body>
        <div class="row">
            <div class="row" id="user-div">
                <div class="col s2 offset-s1 left-align">
                    <p class="">Bienvenido:&nbsp;<span class="teal-text" id="nombre-usuario">nombre usuario</span> </p>
                </div>
                <div class="col s2 offset-s6 right-align" id="salir">
                    <a class="btn grey lighten-3 teal-text" id="exit"><i class="material-icons left teal-text">exit_to_app</i>Salir</a>
                </div>
            </div>
            <div class="row" id="heading-div">
                <div class="col s12 center-align">
                    <h3 class="teal-text">Portal de Mejoras y Reportes de Fallas de Sistemas</h3>
                </div>
            </div>
            <div class="row">
                <div class="col s10 offset-s1 center-align">
                    <h5>Departamento de Desarrollo de Software</h5>
                    <h4>Registro</h4>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 offset-s3">
                    <select id="solicitud">
                        <option value="" disabled selected>Tipo de Solicitud</option>
                        <option value="1" id="mejora">Mejora</option>
                        <option value="2" id="bug">Bug/Falla</option>
                        <option value="3" id="modulo">Nuevo M&oacute;dulo</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6 offset-s3">
                    <textarea id="comentarios" class="materialize-textarea"></textarea>
                    <label for="textarea1">Describa la solicitud</label>
                </div>
            </div>
            <div class="row">
                <br>
                <div class="file-field input-field col s6 offset-s3">
                    <div class="btn grey lighten-3 teal-text">
                        <i class="material-icons teal-text left">photo</i>
                        <span>Adjuntar</span>
                        <input type="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" id="evidencia" type="text">
                        <label for="evidencia">Cargar Archivo/Imagen</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <br><br>
                <div class="col s2 offset-s5 center-align">
                    <a class="waves-effect waves-light btn grey lighten-3 teal-text modal-trigger" href="#modal1">
                        <i class="material-icons right teal-text ">send</i>Enviar
                    </a>
                </div>
                <br>
            </div>
        </div>
        <div id="modal1" class="modal">
            <div class="modal-content center-align">
                <h4>Solicitud Procesada</h4>
                <hr>
                <h5>Tu solicitud ha sido registrada exitosamente</h5>
                <h5 class="teal-text">Pronto te contactaremos</h5>
            </div>
            <div class="modal-footer center-align">
                <a href="#!" class="modal-close waves-effect waves-green green btn-flat">Aceptar</a>
            </div>
        </div>

        <%@include file="jspf/librerias_js.jspf" %>
        <script src="js/portal.js" type="text/javascript"></script>
    </body>
</html>
